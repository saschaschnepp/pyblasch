
# Copyright (c) <2015> <Sascha M Schnepp>
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in
# all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.  IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
# THE SOFTWARE.

###############################################################################

import numpy as np
from abc import abstractmethod

# --------------------- import submodules ---------------------------------- #
from ..output.printers import printers
from ..instruments.options import *
from ..utils.utils import *

# ------ class vars -------
pr = printers()

# -------------------------------------------------------------------------- #
class RK():
  '''Abstract base class for RK integrators'''

  params = {}

  # ------------------------------------------------------------------------ #
  def __init__(self,RKtype='LSRK5_4',dt=-0.00001,silent=False):
    '''Base settings'''

    self.setType(RKtype)
    self.setDt(dt)
    self.setStages(0)
    self.setOrder(0)
    self.setStep(None)    # will contain the stepping function later on
    self.setSilent(silent)

  # ---- abstract methods ----- #
  @abstractmethod
  def step(self,*args):
    pass

  # --------- getters and setters ----------------------------------------------- #
  def getType(self):
    return self.params['type']
  def getDt(self):
    return self.params['dt']
  def getStages(self):
    return self.params['stages']
  def getOrder(self):
    return self.params['order']
  def getStep(self):
    return self.params['step']
  def getRegs(self):
    return self.params['regs']
  def getSilent(self):
    return self.params['silent']

  def setType(self,t):
    self.params['type'] = t
  def setDt(self,t):
    self.params['dt'] = t
  def setStages(self,t):
    self.params['stages'] = t
  def setOrder(self,t):
    self.params['order'] = t
  def setStep(self,t):
    self.params['step'] = t
  def setRegs(self,t):
    self.params['regs'] = t
  def setSilent(self,t):
    self.params['silent'] = t

  # ----------------------------------------------------------------------------- #
  def prtParameters(self):
    iterDict(self.params,'RK integrator initialized.')

  # ----------------------------------------------------------------------------- #

class clRK(RK):
  '''Classic RK integrators'''

  def __init__(self,RKtype='RK4_4',dt=-0.0001,silent=False):
    super().__init__(RKtype,dt,silent=silent)

    if self.getType() is 'RK4_4':
      self.setStages(4)
      self.setRegs(4)
    else:
      pr.prtError('Unknown clRK integrator type ' + str(RKtype),fatal=True)

    # ----------------------------------------------------------------------------- #
    # @TODO: Dummy step function
    def step(self,*args):
      return u

# ----------------------------------------------------------------------------- #
class lsRK(RK):
  '''Low-storage RK integrators'''

  def __init__(self,RKtype='LSRK5_4',dt=-0.0001,silent=False):
    super().__init__(RKtype,dt,silent=silent)

    self.step           = self.stepLSRK
    self.setStep(self.stepLSRK)
    self.setRegs(1) # all ls RK schemes use 1 extra register

    if self.getType() is 'euler':
      self.setStages(1)
      self.setOrder(1)
      self.params['a']      = np.array([0.0])
      self.params['b']      = np.array([1.0])
      self.params['c']      = np.array([0.0])

    elif self.getType() is 'LSSSPRK5_3':
      self.setStages(5)
      self.setOrder(3)
      self.params['a']      = np.array([0.0,
                                        -2.60810978953486,
                                        -0.08977353434746,
                                        -0.60081019321053,
                                        -0.72939715170280])

      self.params['b']      = np.array([0.67892607116139,
                                        0.20654657933371,
                                        0.27959340290485,
                                        0.31738259840613,
                                        0.30319904778284])
      self.params['c']      = np.array([0.0,
                                        0.67892607116139,
                                        0.34677649493991236,
                                        0.6667335950098192,
                                        0.7659008742903154])

    elif self.getType() is 'LSRK5_4':
      self.setStages(5)
      self.setOrder(4)
      self.params['a']      = np.array([0.0,
                                        -567301805773.0/1357537059087.0,
                                        -2404267990393.0/2016746695238.0,
                                        -3550918686646.0/2091501179385.0,
                                        -1275806237668.0/842570457699.0])
      self.params['b']      = np.array([1432997174477.0/9575080441755.0,
                                        5161836677717.0/13612068292357.0,
                                        1720146321549.0/2090206949498.0,
                                        3134564353537.0/4481467310338.0,
                                        2277821191437.0/14882151754819.0])
      self.params['c']      = np.array([0.0,
                                        1432997174477.0/9575080441755.0,
                                        2526269341429.0/6820363962896.0,
                                        2006345519317.0/3224310063776.0,
                                        2802321613138.0/2924317926251.0])

    else:
      pr.prtError('Unknown lsRK integrator type ' + str(RKtype),ftl=True)

    if not self.getSilent():
      self.prtParameters()

  # ----------------------------------------------------------------------------- #
  def stepLSRK(self,*args):
    '''Expects args: rhs=, u=, t=, ...'''

    # For the time stepping of unknown functions (such as in the case of
    # solving the B-S equation) the time is not required. For API consistency
    # the time still needs to passed in.
    if len(args) < 2:
      pr.prtError('RK step function needs at least two arguments')
    rhsF = args[0]
    u    = args[1]
    t0   = args[2]
    reg  = np.zeros(np.shape(u))
    dt   = -self.getDt()
    dt_n = dt/np.timedelta64(365,'D')
    a    = self.params['a']
    b    = self.params['b']
    c    = self.params['c']

    for s in range(self.getStages()):
      t = t0 + c[s]*dt
      if len(args) > 3: # this ugly business is required only for the rk unit test in rk_test.py where u,t and args[1,2] agree only in the first stage
        rhs = rhsF(u,t,*args[3:])
      else:
        rhs = rhsF(u,t)
      reg = a[s]*reg + dt_n*rhs
      u   = u + b[s]*reg

    return np.asarray(u)

  # ----------------------------------------------------------------------------- #
