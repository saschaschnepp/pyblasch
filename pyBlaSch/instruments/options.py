
# Copyright (c) <2015> <Sascha M Schnepp>
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in
# all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.  IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
# THE SOFTWARE.

###############################################################################

import numpy as np
from abc import ABCMeta,abstractmethod
from scipy.interpolate import interp1d

# --------------------- import submodules ---------------------------------- #
from ..output.printers import printers
from ..BlackScholes.BlackScholes import BlackScholes
from ..utils.utils import *

# ------ class vars ------
pr = printers()

# ============================================================================= #
class options(metaclass=ABCMeta):
  '''Implements options '''

  params = {}

  # ------ abstract methods ------
  def __init__(self,silent=False):
    self.setPrice(None)
    self.setSilent(silent)

  @abstractmethod
  def payoff(self,S):
    pass

  @abstractmethod
  def value(self,r,v,d):
    pass

  @abstractmethod
  def PutCallParity(self,r,v,d):
    pass

  # ------ Getters -------
  def getSilent(self):
    return self.params['silent']

  def getKind(self):
    return self.params['kind']

  def getType(self):
    return self.params['type']

  def getPrice(self):
    return self.params['price']

  def getPriceOfS(self):
    return self.params['price(S)']

  def getDelta(self):
    if 'Delta' in self.params:
      return self.params['Delta'],self.params['DeltaOfS']
    else:
      return None

  def getGamma(self):
    if 'Gamma' in self.params:
      return self.params['Gamma'],self.params['GammaOfS']
    else:
      return None

  def getVega(self):
    return self.params['Vega']

  def getSGrid(self):
    return self.params['S grid']

  def getUnderlying(self):
    return self.params['underlying']

  def getExpiry(self):
    return self.params['expiry']

  def getStrike(self):
    return self.params['strike']

  def getType(self):
    return self.params['type']

  def getValue(self):
    return self.params['value']

  def getBCType(self):
    return self.params['BC']

  # ------ Setters -------
  def setSilent(self,v):
    self.params['silent'] = v

  def setKind(self,v):
    self.params['kind'] = v

  def setType(self,v):
    self.params['type'] = v

  def setPrice(self,v):
    self.params['price'] = v

  def setPriceOfS(self,v):
    self.params['price(S)'] = v

  def setDelta(self,v,w):
    self.params['Delta']    = v
    self.params['DeltaOfS'] = w

  def setGamma(self,v,w):
    self.params['Gamma']    = v
    self.params['GammaOfS'] = w

  def setVega(self,v):
    self.params['Vega'] = v

  def setSGrid(self,v):
    self.params['S grid'] = v

  def setUnderlying(self,v):
    self.params['underlying'] = v

  def setExpiry(self,v):
    self.params['expiry'] = v

  def setStrike(self,v):
    self.params['strike'] = v

  def setType(self,v):
    self.params['type'] = v

  def setValue(self,v):
    self.params['value'] = v

  def setBCType(self,v):
    self.params['BC'] = v

  # ------ Greeks -------
  ## ----- Delta
  def Delta(self):

    # check whether previously calculated
    delta = self.getDelta()
    if delta:
      return delta

    S        = self.getSGrid()
    dS       = np.gradient(S)
    deltaOfS = np.gradient(self.getPriceOfS(),dS)*100
    delta    = interp1d(S,deltaOfS,kind='cubic')(self.getUnderlying())

    self.setDelta(delta,deltaOfS)
    return delta,deltaOfS

  ## ----- Gamma
  def Gamma(self):

    delta,deltaOfS = self.getDelta()
    if not delta:
      delta,deltaOfS = self.Delta()

    S        = self.getSGrid()
    dS       = np.gradient(S)
    gammaOfS = np.gradient(deltaOfS/100,dS)
    gamma    = interp1d(S,gammaOfS,kind='cubic')(self.getUnderlying())

    self.setGamma(gamma,gammaOfS)
    return gamma,gammaOfS

  ## ----- Vega
  # @todo Note there is an unresolved issue with the vega. See the Issue Tracker on the bitbucket page
  def Vega(self,r,v,d):

    knd = self.getKind()
    tpe = self.getType()
    stk = self.getStrike()
    und = self.getUnderlying()
    exr = self.getExpiry()

    if knd == 'european':
      op1 = optionsEuropean(tpe,strike=stk,underlying=und,expiry=exr,silent=True)
    elif knd == 'binary':
      op1 = optionsBinary(tpe,strike=stk,underlying=und,expiry=exr,silent=True)
    else:
      pr.prtError('Unknown kind of option: ' + toString(knd))

    pr.prtInfo('++ Vega additional run with volatility + 1%')
    op1.value(r,v+0.01,d,silent=True)
    prc1 = op1.getPrice()
    print(prc1,self.getPrice())

    vega = (prc1-self.getPrice())/(0.01)
    self.setVega(vega)

    return vega


# ============================================================================= #
class optionsEuropean(options):
  '''Implements european calls/puts'''

  # -----------------------------------------------------------------------------
  # def __init__(self,europeanType='call',strike=100,expiry=datetime.datetime.today() + datetime.timedelta(days=2)):
  def __init__(self,europeanType='call',strike=100,underlying=100,expiry=2,BCType='dirichlet',silent=False):

    super().__init__(silent=silent)
    if europeanType not in ['call','put']:
      pr.prtError('Invalid European type: ' + toString(europeanType))

    self.setKind('european')
    self.setType(europeanType)
    self.setPrice(None)
    self.setExpiry(expiry)
    self.setUnderlying(underlying)
    self.setStrike(strike)
    self.setType(europeanType)
    if BCType is 'dirichlet':
      if europeanType is 'call':
        self.setBCType('EuropeanCallDirichlet')
      else:
        self.setBCType('EuropeanPutDirichlet')
    else:
      pr.prtError('Invalid BC type. Type is ' + str(europeanType))

    if not self.getSilent():
      iterDict(self.params,'European option created.')

  # -----------------------------------------------------------------------------
  def payoff(self,S):
    '''Payoff for underlying price S'''

    if self.getType() == 'call':
      return max(0,S-self.getStrike())
    elif self.getType() == 'put':
      return max(0,self.getStrike()-S)
    else:
      pr.prtError("Invalid European option type")

  # -----------------------------------------------------------------------------
  def value(self,r,v,d,silent=False):
    '''Time value of the european'''

    bs = BlackScholes(r,v,d,silent)
    return bs.solve(self)

  # ----- Put-call parity ------
  def PutCallParity(self,r,v,d):
    '''
    Put-Call Parity for European options with annualized dividends C - P = S - (K+D).B with
    C: Call price
    P: Put price
    S: Underlying spot price
    K: Strike price
    D: Annualized dividend * spot price
    B: Present value of an equivalent zero-coupon bond exp[-r(time-to-expiry/Yr)]
    '''

    tpe = self.getType()
    stk = self.getStrike()
    spt = self.getUnderlying()
    exr = self.getExpiry()
    prc = self.getPrice()
    B   = np.exp(-r*exr/np.timedelta64(365,'D'))

    if tpe == 'call':
      price = prc - spt + (stk+d*spt)*B
    else:
      price = prc + spt - (stk+d*spt)*B

    return price

# ============================================================================= #
class optionsBinary(options):
  '''Implements binary calls/puts'''

  # -----------------------------------------------------------------------------
  # def __init__(self,binaryType='call',strike=100,expiry=datetime.datetime.today() + datetime.timedelta(days=2)):
  def __init__(self,binaryType='call',strike=100,underlying=100,expiry=2,BCType='dirichlet',silent=False):

    super().__init__(silent=silent)
    if binaryType not in ['call','put']:
      pr.prtError('Invalid Binary Type!')

    self.setKind('binary')
    self.setType(binaryType)
    self.setPrice(None)
    self.setExpiry(expiry)
    self.setUnderlying(underlying)
    self.setStrike(strike)
    self.setType(binaryType)
    if BCType is 'dirichlet':
      if binaryType is 'call':
        self.setBCType('BinaryCallDirichlet')
      else:
        self.setBCType('BinaryPutDirichlet')
    else:
      pr.prtError('Invalid BC type. Type is ' + str(binaryType))

    if not self.getSilent():
      iterDict(self.params,'Binary option created.')

  # -----------------------------------------------------------------------------
  def payoff(self,S):
    '''Payoff for underlying price S'''

    if self.getType() == 'call':
      return int(S >= self.getStrike())
    elif self.getType() == 'put':
      return int(S < self.getStrike())
    else:
      pr.prtError("Invalid Binary option type")

  # -----------------------------------------------------------------------------
  def value(self,r,v,d,silent=False):
    '''Time value of the binary'''

    bs = BlackScholes(r,v,d,silent)
    return bs.solve(self)

  # ----- Put-call parity ------
  def PutCallParity(self,r,v,d):
    '''
    Put-Call Parity for Binary options with annualized dividends C + P = B with
    C: Call price
    P: Put price
    D: Annualized dividend
    B: Present value of an equivalent zero-coupon bond exp[-r(time-to-expiry/Yr)]
    '''

    exr = self.getExpiry()
    prc = self.getPrice()

    B     = np.exp(-r*exr/np.timedelta64(365,'D'))
    price = B - prc

    return price

# ============================================================================= #
