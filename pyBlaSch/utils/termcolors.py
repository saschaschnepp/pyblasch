
# Copyright (c) <2015> <Sascha M Schnepp>
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in
# all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.  IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
# THE SOFTWARE.

###############################################################################

def green(s):
  return '\033[32m'+s+'\033[0m'

def blue(s):
  return '\033[34m'+s+'\033[0m'

def red(s):
  return '\033[31m'+s+'\033[0m'

def orange(s):
  return '\033[33m'+s+'\033[0m'

def purple(s):
  return '\033[35m'+s+'\033[0m'

def darkgray(s):
  return '\033[30m'+s+'\033[0m'

def lightgray(s):
  return '\033[37m'+s+'\033[0m'

def cyan(s):
  return '\033[36m'+s+'\033[0m'
