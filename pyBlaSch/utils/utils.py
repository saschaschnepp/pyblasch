
# Copyright (c) <2015> <Sascha M Schnepp>
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in
# all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.  IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
# THE SOFTWARE.

###############################################################################

# --------------------- import submodules ---------------------------------- #
from ..output.printers import printers

# ------ class vars ------
pr = printers()

# -----------------------------------------------------------------------------
def iterDict(dct,title=None,printTitle=True,fmt='{:12s} = {: >s}',lastOne=True):
  prefix = 'Content:'
  if title:
    prefix = title + ' ' + prefix
  if printTitle:
    pr.prtInfo(prefix)
  for key,val in sorted(dct.items()):
    if not isinstance(val,dict):
      print('+  ' + fmt.format(str(key),str(val)))
    else:
      iterDict(val,printTitle=False,fmt=str(key)+': {:12s} = {: >s}',lastOne=False)
  if lastOne:
    print('')

# ----------------------------------------------------------------------------- #
def isNumber(s):
  try:
    float(s)
    return True
  except ValueError:
    return False

# ----------------------------------------------------------------------------- #
def stringToList(s,separator):
  return s.split(separator)

# ----------------------------------------------------------------------------- #
def toString(x):
  if isinstance(x,str):
    return x
  elif isinstance(x,bytes):
    return x.decode('utf-8')
  else:
    return str(x)

# ----------------------------------------------------------------------------- #
# Comparing strings ignoring case
# from http://stackoverflow.com/questions/319426/how-do-i-do-a-case-insensitive-string-comparison-in-python

def str_normalize_caseless(text):
  return unicodedata.normalize("NFKD", text.casefold())

def str_equal_ignore_case(left, right):
  return normalize_caseless(left) == normalize_caseless(right)

###############################################################################
