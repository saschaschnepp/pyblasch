
# Copyright (c) <2015> <Sascha M Schnepp>
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in
# all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.  IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
# THE SOFTWARE.

###############################################################################

import numpy as np
from scipy.interpolate import interp1d

# --------------------- import submodules ---------------------------------- #
from ..output.printers import printers
from .. RK import rk
from ..instruments.options import *
from ..utils.utils import *

# ------ class vars -------
pr = printers()

# -------------------------------------------------------------------------- #
class BlackScholes(object):
  '''Base class of a Black-Scholes solver'''

  params = {}
  S  = None
  dS = None

  # ----------------------------------------------------------------------------- #
  def __init__(self,r=0.01,v=0.2,d=0.0,silent=False):
    '''Base settings'''

    self.setR(r)   # risk free interest rate
    self.setV(v)   # volatility
    self.setD(d)   # annulized dividend
    self.setSilent(silent)

    if not self.getSilent():
      self.prtParameters()

  # ======= Boundary Conditions ================================================= #
  def BCEuropeanCallDirichlet(self,V,t,op):
    V[0]  = 0
    V[-1] = self.S[-1] - op.getStrike()*np.exp(-self.getR()*t/np.timedelta64(365,'D'))

    return V

  # ----------------------------------------------------------------------------- #
  def BCEuropeanPutDirichlet(self,V,t,op):
    V[0]  = op.getStrike()*np.exp(-self.getR()*t/np.timedelta64(365,'D'))
    V[-1] = 0

    return V

  # ----------------------------------------------------------------------------- #
  def BCBinaryCallDirichlet(self,V,t,op):
    V[0]  = 0
    V[-1] = 1

    return V

  # ----------------------------------------------------------------------------- #
  def BCBinaryPutDirichlet(self,V,t,op):
    V[0]  = 1
    V[-1] = 0

    return V

  # ----------------------------------------------------------------------------- #

  applyBC = {
    'EuropeanCallDirichlet' : BCEuropeanCallDirichlet,
    'EuropeanPutDirichlet'  : BCEuropeanPutDirichlet,
    'BinaryCallDirichlet'  : BCBinaryCallDirichlet,
    'BinaryPutDirichlet'   : BCBinaryPutDirichlet,
  }

  # ============================================================================= #
  def prtParameters(self):
    iterDict(self.params,'Black-Scholes initialized.')

  # ----------------------------------------------------------------------------- #
  def setR(self,val):
    self.params['r'] = val

  def getR(self):
    return self.params['r']

  # ----------------------------------------------------------------------------- #
  def setSilent(self,val):
    self.params['silent'] = val

  def getSilent(self):
    return self.params['silent']

  # ----------------------------------------------------------------------------- #
  def setV(self,val):
    self.params['v'] = val

  def getV(self):
    return self.params['v']

  # ----------------------------------------------------------------------------- #
  def setD(self,val):
    self.params['d'] = val

  def getD(self):
    return self.params['d']

  # ----------------------------------------------------------------------------- #
  def solve(self,op):

    # grid
    upper = np.amax([op.getStrike(),op.getUnderlying()])
    self.S  = np.linspace(0,2*upper,101)
    self.dS = np.gradient(self.S)

    op.setSGrid(self.S)

    # payoff along grid (Initial values of V)
    po = np.asarray([op.payoff(s) for s in self.S])

    # ----- time stepping ------
    # init integrator
    intg = rk.lsRK(RKtype='LSRK5_4',dt=np.timedelta64(1,'h'),silent=self.getSilent())

    t     = np.timedelta64(0,'D')
    histT = [t]
    V     = po
    histV = np.asarray(V)

    steps = np.abs(op.getExpiry()/intg.getDt())
    inc   = int(steps/50)
    n     = 0
    while t < op.getExpiry():
      if not self.getSilent():
        pr.prtInfo('Solving backward in time: ' + str(int(n/steps*100)) + ' %',end='\r')
      V = intg.step(self.rhs,V,t)

      # apply boundary conditions
      V = self.applyBC[op.getBCType()](self,V,t,op)

      t = t + intg.getDt()
      if not n%inc:
        histV = np.vstack((histV,np.asarray(V)))
        histT.append(t)
      n = n+1

    if not self.getSilent():
      pr.prtInfo('Solving backward in time: 100 %')

    op.setPrice(interp1d(self.S,V,kind='cubic')(op.getUnderlying()))
    op.setPriceOfS(V)
    return self.S,histV,np.asarray(histT)

  # ----------------------------------------------------------------------------- #
  def dVdS(self,V):
    # edge_order was introduced in numpy 1.9.1
    ver = np.asarray(np.version.version.split('.')).astype(np.float) >= np.asarray([1,9,1])
    if np.all(ver):
      return np.gradient(V,self.dS,edge_order=1)
    else:
      return np.gradient(V,self.dS)

  def d2VdS2(self,V):
    # @ATTENTION: Using np.gradient again on the first gradients is an
    # inconsistent discretization and does not work. This solution
    # computes textbook second order differences by evaluating derivatives
    # at the center point between two mesh points and computes the second
    # derivative at the mesh point based on these values. This is a second
    # order centered difference representation.

    # @TODO: Does line conflict with more 'involved' BCs s.a. vanishing diffusion and drift
    V0 = np.concatenate(([V[0]-(V[1]-V[0])], V, [V[-1]+(V[-1]-V[-2])]))
    V0 = np.diff(np.diff(V0))/self.dS/self.dS

    return V0

  # ----------------------------------------------------------------------------- #
  def rhs(self,V,t):

    dvs  = self.dVdS(V)
    d2vs = self.d2VdS2(V)
    r    = self.getR()
    v    = self.getV()
    d    = self.getD()

    return r*V - (r-d)*self.S*dvs - 0.5*v*v*self.S*self.S*d2vs

# --------------------------------------------------------------------------- #
