
# Copyright (c) <2015> <Sascha M Schnepp>
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in
# all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.  IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
# THE SOFTWARE.

###############################################################################

import numpy as np
import matplotlib.cm as cm
import matplotlib.pyplot as plt
from matplotlib.backends.backend_pdf import PdfPages
from matplotlib.ticker import LinearLocator, FormatStrFormatter
from matplotlib import rc
rc('font',**{'family':'serif','serif':['Times New Roman']})
from mpl_toolkits.mplot3d import Axes3D

# ----------------------------------------------------------------------------- #
def plotsurf(S,histV,histT):
  T = np.linspace(0,len(histT),len(histT))

  X, Y = np.meshgrid(S, T)
  Z = histV.flatten().reshape(len(T),len(S))

  fig = plt.figure()
  ax = fig.gca(projection='3d')
  surf = ax.plot_surface(X, Y, Z, rstride=5, cstride=2, cmap=cm.coolwarm,
        linewidth=0, antialiased=False)

  ax.zaxis.set_major_locator(LinearLocator(10))
  ax.zaxis.set_major_formatter(FormatStrFormatter('%.02f'))
  ax.set_xlabel('Underlying price')
  ax.set_ylabel('Time from expiry')
  ax.set_zlabel('Option price')
  ax.yaxis.get_major_ticks(numticks=8)
  stp = len(histT)/7
  txs = np.asarray(histT)[::stp].astype('timedelta64[D]')
  ax.set_yticklabels(txs)

  plt.show()

# ----------------------------------------------------------------------------- #
