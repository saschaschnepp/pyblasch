
# Copyright (c) <2015> <Sascha M Schnepp>
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in
# all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.  IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
# THE SOFTWARE.

###############################################################################

# Implementation of analytical solution for European option taken from
# http://gosmej1977.blogspot.ch/2013/02/black-and-scholes-formula.html

import numpy as np
import matplotlib.cm as cm
import matplotlib.pyplot as plt
from matplotlib.backends.backend_pdf import PdfPages
import scipy.stats as st

# --------------------- import submodules ---------------------------------- #
from pyBlaSch import output
from pyBlaSch import instruments as inst
from pyBlaSch import RK
opts = inst.options
pr = output.printers.printers()

class TestBS():
  '''Unit test of BlackScholes/BlackScholes module'''

  # --------------------------------------------------------------------------- #
  # Black-Scholes analytical solution
  def d1(self,S0, K, r, sigma, T):
    return (np.log(S0/K) + (r + sigma**2 / 2) * T)/(sigma * np.sqrt(T))
 
  def d2(self,S0, K, r, sigma, T):
    return (np.log(S0 / K) + (r - sigma**2 / 2) * T) / (sigma * np.sqrt(T))
 
  def AnalyticalBlackScholesEuropean(self,optype,S0, K, r, sigma, T):
    if optype=="call":
        return S0 * st.norm.cdf(self.d1(S0, K, r, sigma, T)) - K * np.exp(-r * T) * st.norm.cdf(self.d2(S0, K, r, sigma, T))
    else:
       return K * np.exp(-r * T) * st.norm.cdf(-self.d2(S0, K, r, sigma, T)) - S0 * st.norm.cdf(-self.d1(S0, K, r, sigma, T))

  # ----------------------------------------------------------------------------- #
  def run(self):
    '''Run the test'''

    pr.prtInfo('Running ' + self.__class__.__name__)
    pltname = 'test_bs.pdf'

    ###############################################################################
    # CALL
    underlying = 100.0
    strike     = 100.0
    r          = 0.05
    v          = 0.3
    d          = 0.0
    expiry     = np.timedelta64(365,'D')
    optype     = 'call'

    print("Underlying price at time 0             : " + str(underlying))
    print("Strike price                           : " + str(strike))
    print("Continuously compounded risk-free rate : " + str(r))
    print("Volatility of the stock price per year : " + str(v))
    print("Time to maturity                       : " + str(expiry))
    print("Option type                            : " + optype)


    c_BS = self.AnalyticalBlackScholesEuropean(
      optype,underlying,strike,r,v,expiry/np.timedelta64(365,'D'))
    op   = opts.optionsEuropean(optype,strike=strike,underlying=underlying,expiry=expiry,silent=True)
    op.value(r,v,d)
    n_BS = op.getPrice()
                                      
    print("Black-Scholes price (analytical): " + str(np.around(c_BS,decimals=2)))
    print("Black-Scholes price (analytical): " + str(np.around(c_BS,decimals=2)))
    print("")

    ###############################################################################
    # PUT
    underlying = 100.0
    strike     = 100.0
    r          = 0.05
    v          = 0.3
    d          = 0.0
    expiry     = np.timedelta64(365,'D')
    optype     = 'put'

    print("Underlying price at time 0             : " + str(underlying))
    print("Strike price                           : " + str(strike))
    print("Continuously compounded risk-free rate : " + str(r))
    print("Volatility of the stock price per year : " + str(v))
    print("Time to maturity                       : " + str(expiry))
    print("Option type                            : " + optype)

    c_BS = self.AnalyticalBlackScholesEuropean(
      optype,underlying,strike,r,v,expiry/np.timedelta64(365,'D'))
    op  = opts.optionsEuropean(optype,strike=strike,underlying=underlying,expiry=expiry,silent=True)
    op.value(r,v,d)
    n_BS = op.getPrice()
                                      
    print("Black-Scholes price (analytical): " + str(np.around(c_BS,decimals=2)))
    print("Black-Scholes price (numerical) : " + str(np.around(n_BS,decimals=2)))
    print("")
