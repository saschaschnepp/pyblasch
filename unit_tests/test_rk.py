
# Copyright (c) <2015> <Sascha M Schnepp>
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in
# all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.  IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
# THE SOFTWARE.

###############################################################################

import numpy as np
import matplotlib.cm as cm
import matplotlib.pyplot as plt
from matplotlib.backends.backend_pdf import PdfPages
# from matplotlib import rc
# rc('font',**{'family':'serif','serif':['Times New Roman']})

# --------------------- import submodules ---------------------------------- #
from pyBlaSch.output.printers import printers
from pyBlaSch.RK.rk import *

class TestRK():
  '''Unit test of RK.rk module'''

  pr = printers()

  # ----------------------------------------------------------------------------- #
  # test 1 taken from:
  # https://joshcashaback.wordpress.com/2012/01/03/numerical-integration-runge-kutta-and-euler-method/
  def sol_1(self,t):
    return 10*np.exp(-0.4*t/np.timedelta64(1,'D')) - 5*np.exp(-t/np.timedelta64(1,'D'))
  def rhs_1(self,*args):
    u = args[0]
    t = args[1]
    return 3*np.exp(-t/np.timedelta64(1,'D')) - 0.4*u

  # ----------------------------------------------------------------------------- #
  def sol_2(self,t):
    return np.sin(t/np.timedelta64(1,'D'))
  def rhs_2(self,*args):
    t = args[1]
    return np.cos(t/np.timedelta64(1,'D'))

  # ----------------------------------------------------------------------------- #
  def run(self,RKtype=['LSRK5_4']):
    '''Run the test'''

    pr.prtInfo('Running ' + self.__class__.__name__ + ' using RK type(s) ' + str(RKtype))
    pltname = 'test_rk.pdf'
    rhs = self.rhs_2
    sol = self.sol_2
    steps = [11,100,1000]

    u0_1 = 5
    u0_2 = 0

    with PdfPages(pltname) as pp:
        Tf  = 38
        tit = None

        for rkt in RKtype:
          tit = rkt
          U   = []
          T   = []
          S   = []
          d   = []
          for i,m in enumerate(steps):
            rk  = lsRK(rkt,dt=np.timedelta64(int(np.ceil(Tf/m)),'D'))
            dt  = rk.getDt()
            d.append(dt)

            u   = u0_1
            t   = np.timedelta64(0,'D')
            U.append([u])
            T.append([0])
            S.append([u])

            for n in np.linspace(1,m,m):
              u = rk.step(rhs,u,t)
              t = n*dt
              T[i].append(t)
              U[i].append(u)
              S[i].append(sol(t))

          plt.clf()
          for i in range(len(steps)):
            plt.subplot(len(steps),1,i+1)
            plt.title(tit + '_' + str(d[i]))
            plt.plot(T[i],U[i],color='r',marker='.')
            plt.plot(T[i],S[i],color='g',marker='o')

          # plt.show()
          try:
            plt.savefig(pp,format='pdf')
          except IOError:
            pr.prtError('Cannot write to ' + pltname)
          else:
            pr.prtInfo(" +++ Saved figure: " + tit + " to " + pltname)

