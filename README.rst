pyBlaSch - An object-oriented Python code for option pricing with the Black-Scholes equation
======
.. default-role:: math
The Black-Scholes equation

`\frac{\partial V}{\partial t} + \frac{1}{2}\sigma^2 S^2 \frac{\partial^2 V}{\partial S^2} + rS\frac{\partial V}{\partial S} - rV = 0`

is the basis of option pricing. It is a parabolic partial differential equation involving the option price `V,` the price of the underlying stock `S,` the volatility `\sigma,` and the risk free rate `r.` As it is simple to include underlying (annualized) dividend payments `d`, these are included in the code, too.

.. image:: https://bitbucket.org/repo/EMyMLG/images/3358635157-pyBlaSch_s.png


A Word of Warning
------

This code is meant to be used for educational purposes. It is not meant to be used for making money. Trying to do so, may lead to losses, including the total loss of your investment.

This code is open source under the MIT License <https://bitbucket.org/saschaschnepp/pyblasch/src/46ee52122b28c82fbf61938632e935e7e053fff5/LICENSE?at=master&fileviewer=file-view-default>.

`\,`

Implementation
------

* Finite differences discretization of the spatial derivative operators
* Runge-Kutta schemes for the integration in time (currently first to 4th order low-storage schemes)
* Currently only simple Dirichlet type boundary conditions are implemented
* Types of options are European and Binary call/put
* Derived quantities: Greeks Delta and Gamma, Put-Call parity implied price

The code design follows an object-oriented programming paradigm and was done with extensibility in mind. Specifying a different type of contract in many cases should be as simple as defining the payoff, which comes down to a few lines of code. This is the payoff for a European option

  def payoff(self,S):
    '''Payoff for underlying price S'''

    if self.getType() == 'call':
      return max(0,S-self.getStrike())
    elif self.getType() == 'put':
      return max(0,self.getStrike()-S)
    else:
      pr.prtError("Invalid European option type")

`\,`

How do I get set up?
----

* The code requires Python 3 with Numpy, SciPy, and matplotlib
* Clone the code using ``git``. See https://confluence.atlassian.com/bitbucket/clone-a-repository-223217891.html if you require help on how to clone.
* To get started type ``python3 pyBlaSch.py help``

`\,`

Contribution guidelines
----

I am happy for contributions. If you want to contribute make a fork, code your contribution and send me a pull request. Please try to keep simplicity in mind. For contribution ideas see the issues tracker at <https://bitbucket.org/saschaschnepp/pyblasch/issues?status=new&status=open>.

`\,`

Known issues and workarounds
----
* If you encounter this error ``ValueError: unknown locale: UTF-8`` add the following to your ``~/.bash_profile`` and type ``source ~/.bash_profile``:

    export LC_ALL=en_US.UTF-8

    export LANG=en_US.UTF-8
* If you encounter this error ``UnicodeEncodeError: 'ascii' codec can't encode character '\u2202' in position 8: ordinal not in range(128)`` set your terminal character encoding to unicode. As printing of unicode characters occurs only within the 'help' text, you can also delete this argument.