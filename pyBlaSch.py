#!/usr/bin/env python3

# Copyright (c) <2015> <Sascha M Schnepp>
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in
# all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.  IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
# THE SOFTWARE.

# --------------------- import packages ------------------------------------ #
import sys
import numpy as np

# ----- require python3 ---------------------------------------------------- #
version = sys.version_info
if version.major < 3:
  raise Exception("Python3 required")

# --------------------- import submodules ---------------------------------- #
from pyBlaSch.output.printers import printers
from pyBlaSch.output.plotters import plotsurf
from pyBlaSch.instruments.options import *
from pyBlaSch.utils.utils import *
from pyBlaSch.utils.termcolors import *

# ------ class vars -------
pr = printers()
if version.minor < 3:
  pr.prtWarning('''
  !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
  Your version of Python3 is 3.2 or lower. If you experience issues
  such as errors due to failed module import or missing names/functions
  please update your Python installation to a version >=3.3.
  Python3.3 introduced changes regarding submodules which should solve
  these issues.
  !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
  ''')

# -------------------------------------------------------------------------- #
# @todo Write an actual help function (or not if unnecessary)
def help():
  print(purple("\n============================================================================="))
  print(purple("  HELP! I need somebody... HELP!"))
  print(purple("-----------------------------------------------------------------------------"))
  print(purple("  Simple option valuator based on the Black-Scholes equation "))
  print(purple("  solved using Finite Differences and Runge-Kutta time integration."))
  print(purple(u"   \u2202V   1     \u2202\u00B2V      \u2202V"))
  print(purple(u"   -- + -\u03C3\u00B2S\u00B2 --- + rS -- - rV = 0"))
  print(purple(u"   \u2202t   2     \u2202S\u00B2      \u2202S"))
  print(purple("  "))
  print(purple("  Command line options:"))
  print(purple("    help     -- Print this help text"))
  print(purple("    plotsurf -- Plot the option price history as a 3d surface plot"))
  print(purple("=============================================================================\n"))

# --------------------------------------------------------------------------- #
def main():
  if 'help' in sys.argv:
    help()

  r      = 0.05                    # risk free rate
  v      = 0.3                     # volatility
  d      = 0.01                    # annualized dividend
  expiry = np.timedelta64(365,'D') # expiry from now (see http://docs.scipy.org/doc/numpy/reference/arrays.datetime.html for details on how to specify time deltas)

  op = optionsEuropean('call',strike=130,underlying=100,expiry=expiry)
  # op = optionsEuropean('put',strike=130,underlying=100,expiry=expiry)
  # op = optionsBinary('call',strike=100,underlying=100,expiry=expiry)
  # op = optionsBinary('put',strike=100,underlying=100,expiry=expiry)
  S,histV,histT = op.value(r,v,d)

  pr.prtInfo('{: <55}'.format('Option value:') + '{: >7}'.format(str(np.around(op.getPrice(),decimals=2))))

  # # Greeks
  # pr.prtInfo('Computing Greeks:')
  # pr.prtInfo('+ Delta')
  # delta,deltaOfS = op.Delta()
  # pr.prtInfo('+ Gamma')
  # gamma,gammaOfS = op.Gamma()

  # pr.prtInfo('{: <55}'.format('|Delta| at current price and time (scaled to 100):') + '{: >7}'.format(str(np.around(np.absolute(delta),decimals=2))))
  # pr.prtInfo('{: <55}'.format('Gamma at current price and time:') + '{: >7}'.format(str(np.around(gamma,decimals=4))))

  # # Put-Call Parity
  # pcp = op.PutCallParity(r,v,d)
  # pr.prtInfo('{: <55}'.format('Put-Call Parity implied price: ') + '{: >7}'.format(str(np.around(pcp,decimals=2))))

  if 'plotsurf' in sys.argv:
    plotsurf(S,histV,histT)

# --------------------------------------------------------------------------- #
if __name__ == "__main__":
  main()
